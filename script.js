const deck = document.querySelector('.deck');
const restart = document.querySelector('.restart');
const nextRoundButton = document.querySelector('.next-round-button');

let clickable = true;

let firstCard = null;
let firstCardUrl = null;
let secondCard = null;
let secondCardUrl = null;

let maxMatches = 8;
let totalMatches = null;
let attempts = 0;


let counter;
let timeRemaining = 200;

let countDown = null;

let cardsArray = [
    "url(./assets/images/aquaman.jpg)",
    "url(./assets/images/batman.jpg)",
    "url(./assets/images/blackAdam.jpg)",
    "url(./assets/images/flash.jpg)",
    "url(./assets/images/greenLantern.jpg)",
    "url(./assets/images/martianManhunter.jpg)",
    "url(./assets/images/superman.jpg)",
    "url(./assets/images/wonderwoman.jpg)",
    "url(./assets/images/cyborg.jpg)"
];

let bestScore;

function getBestScore() {
    bestScore = Number(localStorage.getItem('bestScore'));

    const bestScoreCount = document.querySelector('.best-score-count');
    bestScoreCount.innerText = bestScore;
}

function generateCardDivs(cardsArray) {

    const allCardsDiv = cardsArray.map((cards) => {

        const cardDiv = document.createElement('div');
        cardDiv.className = 'card';

        const frontDiv = document.createElement('div');
        frontDiv.className = 'front';
        frontDiv.style.backgroundImage = cards;

        const backDiv = document.createElement('div');
        backDiv.className = 'back';
        cardDiv.appendChild(frontDiv);
        cardDiv.appendChild(backDiv);

        cardDiv.addEventListener('click', (event) => {
            handleCardClick(event);
        });

        return cardDiv;
    });

    deck.append(...allCardsDiv);
}

function shuffleArray(array) {
    let currentIndex = array.length;
    let temporaryValue, randomIndex;

    function shuffle(currentIndex) {
        if (currentIndex <= 0) {
            return;
        } else {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
            shuffle(currentIndex - 1);
        }
    }
    shuffle(currentIndex);

    return array;
}

function setGameTable(deck) {
    let shuffledArray = shuffleArray(deck);
    let gameDeck = shuffledArray.slice(0, 8);
    gameDeck = gameDeck.concat(gameDeck);
    gameDeck = shuffleArray(gameDeck);
    generateCardDivs(gameDeck);
}

function removePreviousDeck() {
    const cardDeck = document.querySelector('#card-deck');
    cardDeck.innerText = '';
}

function resetCardValues() {
    firstCard = null;
    firstCardUrl = null;
    secondCard = null;
    secondCardUrl = null;
}

function resetTimer() {
    clearInterval(counter)
    timeRemaining = 200;
    const countDown = document.querySelector('#countdown');
    countDown.innerText = `${timeRemaining} sec`;
}

function resetStats() {
    totalMatches = null;
    attempts = 0;
    resetTimer();
}

function updateStats() {

    const modalAttempts = document.querySelector('.modal-attempts');
    modalAttempts.innerText = `You made ${attempts} attempts`;

    const attemptsCount = document.querySelector('.attempts-count');
    attemptsCount.innerText = attempts;

    const modalTime = document.querySelector('.modal-time');
    modalTime.innerText = ` in ${200 - timeRemaining} seconds`;

    const modalBestScore = document.querySelector('.modal-best-score');
    modalBestScore.innerText = 'Best Score = ' + bestScore;
}

function closeResultModal() {
    const popUpShadow = document.querySelector('#popup-shadow');
    popUpShadow.classList.add('hide');
}

function closeStartModal() {
    const playButtonShadow = document.querySelector('#play-button-shadow');
    resetStats();

    playButtonShadow.classList.add('hide');
}

function openResultModal() {
    clearInterval(counter);
    updateStats();

    const popUpShadow = document.querySelector('#popup-shadow');
    popUpShadow.classList.remove('hide');
}

function gameOver() {
    if (totalMatches !== maxMatches || timeRemaining <= 0) {
        updateStats();
        const nextRoundButton = document.querySelector('.next-round-button');
        nextRoundButton.innerText = "Try Again";

        openResultModal();
    } else {
        openResultModal();
    }
}

function startTimer() {
    timeRemaining = 200;

    counter = setInterval(() => {
        const countDownContainer = document.querySelector('#countdown');
        countDownContainer.innerText = timeRemaining + " sec";
        timeRemaining -= 1;
    }, 1000);

    clearTimeout(countDown);
    countDown = setTimeout(() => {
        clearInterval(counter);
        gameOver();
    }, (200 * 1000));
}

function displayGameResult() {
    clearInterval(counter);
    updateStats();
    const nextRoundButton = document.querySelector('.next-round-button');
    nextRoundButton.innerText = 'Play Again';

    openResultModal();
}

function handleCardClick(event) {
    if (clickable) {

        const clickCard = event.currentTarget;
        clickCard.classList.toggle('isFlipped');

        if (!firstCard) {
            firstCard = clickCard;

            if (attempts <= 0) {
                startTimer();
            }

            const front = clickCard.querySelector('.front');
            firstCardUrl = front.style.backgroundImage;
            clickCard.style.pointerEvents = 'none';

        } else if (!secondCard) {

            secondCard = clickCard;
            const front = clickCard.querySelector('.front');
            secondCardUrl = front.style.backgroundImage;
            clickCard.style.pointerEvents = 'none';
        } else {
            clickable = false;
        }

        attempts += 1;
        const attemptsCount = document.querySelector('.attempts-count');
        attemptsCount.innerText = attempts;

        if (firstCardUrl && secondCardUrl) {
            if (firstCardUrl !== secondCardUrl) {
                clickable = false;
                setTimeout(() => {
                    firstCard.classList.toggle('isFlipped');
                    secondCard.classList.toggle('isFlipped');
                    firstCard.style.pointerEvents = '';
                    secondCard.style.pointerEvents = '';
                    resetCardValues();
                    clickable = true;
                }, 1000);
            } else if (firstCardUrl === secondCardUrl) {
                totalMatches += 1;
                resetCardValues();

                clickable = false;

                setTimeout(() => {
                    if (totalMatches === maxMatches) {
                        if (bestScore === 0 || attempts < bestScore) {
                            bestScore = attempts;
                            localStorage.setItem('bestScore', bestScore);
                        } else {
                            getBestScore();
                        }
                        displayGameResult();
                        clickable = true;
                    } else {
                        clickable = true;
                    }
                }, 1000);
            } else {
                return;
            }
        } else {
            return;
        }
    } else {
        getBestScore();
    }
}

function startGame(deck) {
    removePreviousDeck();
    resetTimer();
    resetStats();
    resetCardValues();
    updateStats();
    closeResultModal();
    closeStartModal();
    getBestScore();
    setGameTable(deck);
}

function openStartModal() {
    const playButtonShadow = document.querySelector('#play-button-shadow');
    playButtonShadow.classList.remove('hide');
}

function popupRestartModal() {
    const popUpShadowRestart = document.querySelector('#popup-shadow-restart');
    popUpShadowRestart.classList.remove('hide');

    const responseYes = document.querySelector('.yes');
    const responseNo = document.querySelector('.no');

    responseYes.addEventListener('click', () => {
        popUpShadowRestart.classList.add('hide');
        startGame(cardsArray);
    });

    responseNo.addEventListener('click', () => {
        popUpShadowRestart.classList.add('hide');
    });
}

function startApp() {
    openStartModal();

    const playButtonShadow = document.querySelector('#play-button-shadow');
    const playButton = document.querySelector('.play-button');

    playButton.addEventListener('click', () => {
        playButtonShadow.style.display = 'none';

        startGame(cardsArray);
    });

    nextRoundButton.addEventListener('click', () => {
        startGame(cardsArray);
    });

    restart.addEventListener('click', () => {
        if (clickable) {
            popupRestartModal();
        } else {
            getBestScore();
        }
    });

}

startApp();